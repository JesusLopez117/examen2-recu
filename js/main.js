var num = 0;
var promedio = 0.0; 

//Función de generar datos aleatorios
const generar = () =>{
    //Obtener los datos
    let edad = Math.floor(Math.random() * (99 - 18) + 18);
    let altura = Math.random() * (2.5 - 1.5) + 1.5;
    let peso = Math.random() * (130 - 20) + 20;

    document.getElementById('edad').value = (edad).toFixed(0);
    document.getElementById('altura').value = (altura).toFixed(2);
    document.getElementById('peso').value = (peso).toFixed(2);

}

//Función para calcular
const calcular = () =>{
    //Obtencion de datos
    let edad = parseInt(document.getElementById('edad').value);
    let altura = parseFloat(document.getElementById('altura').value);
    let peso = parseFloat(document.getElementById('peso').value);
    let imc = 0.0;
    let nivel = "";

    //Verificacion de datos
    if(isNaN(edad) || isNaN(altura) || isNaN(peso)){
        return alert("Los datos son invalidos");
    }

    //Calcular el IMC
    imc = peso / (Math.pow(altura,2));

    //Verificar el nivel
    if(imc < 18.5){
        nivel = "Bajo Peso";
    }else if(imc >= 18.5 && imc <= 24.9){
        nivel = "Peso Saludable";
    }else if(imc >= 25 && imc <= 29.9){
        nivel = "Sobrepeso";
    }else{
        nivel = "Obesidad";
    }

    //Pasar los datos
    document.getElementById('imc').value = (imc).toFixed(2);
    document.getElementById('nivel').value = nivel;
}

//Función para registrar los datos
const registrar = () =>{
    num = num + 1;
    let fila = document.getElementById('fila');
    let imc = parseFloat(document.getElementById('imc').value);
    const registro = {
        edad: document.getElementById('edad').value,
        altura: document.getElementById('altura').value,
        peso: document.getElementById('peso').value,
        imc: document.getElementById('imc').value,
        nivel: document.getElementById('nivel').value
    };

    fila.innerHTML += num + "  " + registro.edad + "  " + registro.altura + "  "
    + registro.peso + "  " + registro.imc + "  " + registro.nivel + "<br>";

    document.getElementById('edad').value = "";
    document.getElementById('altura').value = "";
    document.getElementById('peso').value = "";
    document.getElementById('imc').value = "";
    document.getElementById('nivel').value = "";

    promedio = promedio + imc;
    document.getElementById('imcPro').innerHTML= (promedio).toFixed(2);
}

//Funcion borrar registros y limpiar
const limpiar = () =>{
    document.getElementById('edad').value = "";
    document.getElementById('altura').value = "";
    document.getElementById('peso').value = "";
    document.getElementById('imc').value = "";
    document.getElementById('nivel').value = "";
    document.getElementById('fila').innerHTML = "";
    num = 0;
    document.getElementById('imcPro').innerHTML="";
}

//Botones
document.getElementById('genera').addEventListener('click',generar);
document.getElementById('calcular').addEventListener('click', calcular);
document.getElementById('registrar').addEventListener('click', registrar);
document.getElementById('borrar').addEventListener('click',limpiar);